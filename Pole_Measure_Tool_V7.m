%Pole measurement designed for HOA collection
%October 11th 2018
%Brian Fischer
%Encepta

%Only for point data collection to be stored in DataCell. actual
%measurement calculation and image creation will be done in other functions

%DataCell = Pole_Measure_Tool_V7('0758','9411',"1234567",'7654321',0,'','save');

function [DataCell,I1] = Pole_Measure_Tool_V7(LeftPicture,RightPicture,PoleID,ProjectNumber,PhotoEnhancment,MarkerHeight,saveproj)

% clear all;
warning off;

%% Inputs

% LeftPicture     =	'1234'          ; %Left Photo Number
% RightPicture    =	'4321'          ; %Right Photo Number
% 
% PoleID          =	"1234567"       ;
% 
% 
% 
% PhotoEnhancment =   0               ; % 0: nothing, 1:Hist filter, #>10: that much increase in brightness, best is around 35
% 
% ProjectNumber   =   '7654321'       ; %the seven digit project number
% 
% MarkerHeight    =   0               ; %If a marker was used to define the datum
% 
% Comments        =   ''              ;


%% Load the Calibration and Images
stereoParams = load('stereoParams'); %Loads the calibration data (still needed for distance obtaining)
stereoParams = stereoParams.stereoParams;

PIC1 = sprintf('Left/GOPR%s.JPG',LeftPicture);
PIC2 = sprintf('Right/GOPR%s.JPG',RightPicture);

infoPIC1 = imfinfo(PIC1);
NativeResolution = '3648 x 2736 Pixels';
TimeStamp= infoPIC1.DateTime;  
NameofOperator= 'Brian Fischer';


load(sprintf('DataCell_%s.mat',ProjectNumber)); 


%% Start Row Search >> Load images >> Start menu
checkflag=0;
for i = 1:length(DataCell)
   if strcmp(DataCell{i,1},PoleID)
       checkflag=1;
       
    DataCell{i,101}=PIC1;
    DataCell{i,102}=PIC2;
    DataCell{i,103}=TimeStamp;
    DataCell{i,104}=NativeResolution;
    DataCell{i,105}=NameofOperator;
    DataCell{i,106}=MarkerHeight;
    
 
       
%Read images and apply filters        
I1 = imread(PIC1); % reads image left
I2 = imread(PIC2); % reads image right

if PhotoEnhancment == 0 
    J1 = I1;
    J2 = I2;
end

if PhotoEnhancment == 1 
    J1 = histeq(I1);
    J2 = histeq(I2);
end        
            
if PhotoEnhancment > 1
    J1 = I1+PhotoEnhancment;
    J2 = I2+PhotoEnhancment;
end
clear I2 PhotoEnhancment;

%Start Point Picking
mainmenu = input('1-Start, 2-Overwrite [1]', 's');
if isempty(mainmenu)
    mainmenu = '1';
end

if mainmenu == '1'
    stayinmenu = 1;
    while stayinmenu == 1
    startmenu = input('0-Finished, 1-Default, 2-Copy, What are you measuring?: [1]: ','s');
        if isempty(startmenu)
            startmenu = '1';
        end
        if startmenu == '0'
            stayinmenu = 0;
        end
        
        if strcmp(startmenu,'2')
            foundflag1 = 0;
            copyitem = input('Copy - What are you measuring?: ', 's');
            for colcount = 1:size(DataCell, 2)
            if strcmp(copyitem, DataCell{2,colcount})
            foundflag1 = 1;
            if ~isempty(DataCell{i,colcount})
                disp('Measurment already taken please add a 1 or use overwrite');
            else
                copyfrom = input('Copy - What measurement are you copying?: ', 's');
                foundflag2 = 0;           
                for colcount2 = 1:size(DataCell, 2)
                    if strcmp(copyfrom, DataCell{2,colcount2})
                    foundflag2 = 1;
                    if isempty(DataCell{i,colcount2})
                    disp('Measurment empty! ');
                    else
                        DataCell{i,colcount}=DataCell{i,colcount2};
                        DataCell{i,colcount+1}=DataCell{i,colcount2+1};
                        DataCell{i,colcount+2}=DataCell{i,colcount2+2};
                        disp('copied ');
                    end
                    else
                        %not found
                        if foundflag2 == 0 && colcount2 == size(DataCell, 2) 
                        %add new column
                        disp('copy type not found!');
                   
                        end%ends add col func call
                    end
                end
            
            end %ends check if empty and 
            else
               if foundflag1 == 0 && colcount == size(DataCell, 2) 
                %add new column
                   disp('not found!');
                   
               end%ends add col func call
                
            end %ends if else statement
            end    
        end %ends string comparison for copy
        
        if ~strcmp(startmenu,'1') && ~strcmp(startmenu,'2') && ~strcmp(startmenu,'0')
            foundflag = 0;
        for colcount = 1:size(DataCell, 2)
            if strcmp(startmenu, DataCell{2,colcount})
            foundflag = 1;
            if ~isempty(DataCell{i,colcount})
                disp('Measurment already taken please add a 1 or use overwrite');
            else
               [PickDistance, Pick1, Pick2 ] = Measure_Points( J1, J2, stereoParams);
    
                DataCell{i,colcount}=PickDistance;
                DataCell{i,colcount+1}=Pick1;
                DataCell{i,colcount+2}=Pick2;
                
                
            end %ends check if empty and 
            else
               if foundflag == 0 && colcount == size(DataCell, 2) 
                %add new column
                   disp('not found!');
                   
               end%ends add col func call
                
            end %ends if else statement
        end
        end
        
        %if startmenu == 2   %resevered for back logic
        
        
    end %ends loop and exits the menu and ends the function
end %end of start

%%Overwrite
if mainmenu == '2'
    stayinmenu = 1;
    while stayinmenu == 1
    startmenu = input('0-Finished, 1-Reserved, 2-Copy, What are you Overwriting?: [1]: ','s');
        if isempty(startmenu)
            startmenu = '1';
        end
        if startmenu == '0'
            stayinmenu = 0;
        end
        
        if strcmp(startmenu,'2')%Copy
            foundflag1 = 0;
            copyitem = input('Copy - What are you measuring?: ', 's');
            for colcount = 1:size(DataCell, 2)
            if strcmp(copyitem, DataCell{2,colcount})
            foundflag1 = 1;
            if ~isempty(DataCell{i,colcount})
                copyfrom = input('Copy - What measurement are you copying?: ', 's');
                foundflag2 = 0;           
                for colcount2 = 1:size(DataCell, 2)
                    if strcmp(copyfrom, DataCell{2,colcount2})
                    foundflag2 = 1;
                    if isempty(DataCell{i,colcount2})
                    disp('Measurment empty! ');
                    else
                        DataCell{i,colcount}=DataCell{i,colcount2};
                        DataCell{i,colcount+1}=DataCell{i,colcount2+1};
                        DataCell{i,colcount+2}=DataCell{i,colcount2+2};
                        disp('copied ');
                    end
                    else
                        %not found
                        if foundflag2 == 0 && colcount2 == size(DataCell, 2) 
                        %add new column
                        disp('copy type not found!');
                   
                        end%ends add col func call
                    end
                end
            else
                disp('Measurment empty, cant overwrite');  
                
            
            end %ends check if empty and 
            else
               if foundflag1 == 0 && colcount == size(DataCell, 2) 
                %add new column
                   disp('not found!');
                   
               end%ends add col func call
                
            end %ends if else statement
            end    
        end %ends string comparison for copy
        
        if ~strcmp(startmenu,'1') && ~strcmp(startmenu,'2') && ~strcmp(startmenu,'0')
            foundflag = 0;
        for colcount = 1:size(DataCell, 2)
            if strcmp(startmenu, DataCell{2,colcount})
            foundflag = 1;
            if ~isempty(DataCell{i,colcount})
                [PickDistance, Pick1, Pick2 ] = Measure_Points( J1, J2, stereoParams);
    
                DataCell{i,colcount}=PickDistance;
                DataCell{i,colcount+1}=Pick1;
                DataCell{i,colcount+2}=Pick2;
            else
                disp('Measurment missing!');
               
                
                
            end %ends check if empty and 
            else
               if foundflag == 0 && colcount == size(DataCell, 2) 
                %add new column
                   disp('not found!');
                   
               end%ends add col func call
                
            end %ends if else statement
        end
        end
        
        %if startmenu == 2   %resevered for back logic
        
        
    end %ends loop and exits the menu and ends the function
end %end of start



   end %Ends if string comparison
end %Ends for loop

%% add pole ID if not on the list
if i == length(DataCell) && ~strcmp(DataCell{i,1},PoleID) && checkflag == 0 
       TEMPID{1}=PoleID;
       DataCell(length(DataCell)+1,1)=TEMPID;
       disp('Pole Added: Run Again');
end      

if strcmp(saveproj, 'save')
save(sprintf('DataCell_%s.mat',ProjectNumber), 'DataCell');
disp('Saved ');
disp(PoleID);
else
    disp('Not Saved!')
end



end %Function end: only use if script is being run as a function