clear all;
warning off;

%% Load the Calibration and Images
stereoParams = load('stereoParams'); %Loads the calibration data 
stereoParams = stereoParams.stereoParams;


PIC1 = 'Left/GOPR0084.JPG';
PIC2 = 'Right/GOPR0089.JPG';



I1 = imread(PIC1); % reads image left
I2 = imread(PIC2); % reads image right

%Hist Filter
% J1 = histeq(J1); 
% J2 = histeq(J2);

[J1, J2] = rectifyStereoImages(I1, I2, stereoParams);

anaglyph = stereoAnaglyph(J1,J2);
%imshow(anaglyph);

disparityRange = [-320 320]; %difference divisible by 16 dpends on the disparity of the object you're trying to measure
blockSize = 5                                                                                                                                                                                                                                                                                                                                                                                                                                                          ; %5-255 (15)
contrastThreshold = 0.3; %0 to 1 (0.5)
uniquenessThreshold = 1; %(15)
distanceThreshold = 0; %([])
textureThreshold = 0.0009; %0 to 1 (0.0002) (0.09)

% 'Method',  'BlockMatching',...
disparityMap = disparity(rgb2gray(J1),rgb2gray(J2),... 
    'BlockSize', blockSize,...
    'DisparityRange',disparityRange,...
    'ContrastThreshold', contrastThreshold,...
    'UniquenessThreshold', uniquenessThreshold,...
    'DistanceThreshold', distanceThreshold,...
    'TextureThreshold', textureThreshold);

disparityMap= imrotate(disparityMap,-90);
  
figure 
imshow(disparityMap,disparityRange);
title('Disparity Map');
colormap(gca, jet) 
colorbar