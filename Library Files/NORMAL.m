function [ clear ] = NORMAL( point1, point2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if isempty(point1)
clear = 0;
else
    if isempty(point2)
        clear = 0;
    else
clear = norm(abs(point1 - point2))/1000;
end
end
