function [point3d, Pick1, Pick2 ] = Measure_Points( J1, J2, stereoParams)
fig = figure('units','normalized','outerposition',[0 0 1 1]);
    zoom on;

    imshow(J1,'InitialMagnification','fit');
    Pick1 = ginput2(1);
    imshow(J2,'InitialMagnification','fit');
    Pick2 = ginput2(1);
    
    point3d = triangulate2(Pick1,Pick2, stereoParams);
    %Distance = norm(point3d)/1000;
close(fig);
end

