function [ data ] = ReadGPSData2(type, file, start)

%% GPX reader requires mapping toolbox
% p = gpxread2('20180313-163843.gpx');
% p2 = gpxread2('20180314-100248.gpx');
% Lat = p.Latitude;
% Lon = p.Longitude;
% geoshow(Lat,Lon,'DisplayType','line','Color','red');
% grid on;


%% NMEA Grabs only the GPGGA data. Saves the time and Lat/Long

if type == 'nema'

[fid,errorMessage] = fopen(file);
% line = cell(1,1);
data = cell(1,3);
n = 1;
        while 1 
           tline = fgetl(fid);
           if ~ischar(tline), break, end
                      
           Q = nmealineread2(tline);
           if Q.BODCTime ~=0
           data{n,1} = Q.BODCTime;
           data{n,2} = Q.latitude;
           data{n,3} = Q.longitude;
           n=n+1;
           end
           %line{n,1} = tline;
           
       end
       fclose(fid);
% 
% %        dataRINEX = load('BRN3073M.pos');
%        [fid2,errorMessage2] = fopen('BRN3073M.pos');
%        
%        tline2 = fgetl(fid2);
%        dataPOS = POSread(fid2);  
%        fclose(fid2);
       
       %disp(tline2);
       
% Q = nmealineread2(line);
% time = Q.BODCTime;
% lat = Q.latitude;
% long = Q.longitude;

else 
% start = 17;
data = POSread2(file,start,inf);
%fields = textscan(tline,'%s','delimiter',',');



    

end
%% For Bodc time to 24 hour clock 
% time = 24*time;
% timeMin = rem(time,1);
% timeHour = time-timeMin;
% timeMin = timeMin*60;
% timeSec = rem(timeMin,1);
% timeMin = timeMin - timeSec;
% timeSec = timeSec*60;


%fclose('all');