function  errorcode = DBWrite(DB, TableName, DataTable, varargin);
% DBWrite    Write a matrix or a cell matrix in a table
%
% errorcode = DBWrite(DB, TableName, DataTable);
% errorcode = DBWrite(DB, TableName, DataTable, option);
%
% DB is obtained with DBOpen.
% TableName is a string.
% DataTable must contain the data formated for the table, i.e. ALL columns
% of the table must be provided. Usually, it will be a cell array, but it
% can be a matrix if the table contains only numerical data.
% Option is either 'Append' or 'Replace'. If format is 'Append' (default), the
% data is appended to the Table. If format is 'Replace', the table is first
% deleted, and the data is then written.
% errorcode is zero for successful operation, and negative for errors.
%
% The behaviors for fields with enforced integrity (usually keys, format
% "Auto-Number" in MS Access) is the following:
%   If the key field to write is unique, the line is added
%   If it is pre-existent, the addition is ignored.
%
% (c) 2003, CSE; see library header for more information.

if nargin < 4
    formatstr = 'Append';
else
    formatstr = varargin{1};
end

if strcmpi(formatstr, 'Replace')
    % delete all table
    DB.Execute(['DELETE FROM ' TableName]);
end

errorcode = 0;

for i = 1:size(DataTable,1)
    SQLCommand = ['INSERT INTO ' TableName ' VALUES ('];
    for j = 1:size(DataTable,2)
        if iscell(DataTable)
            data = DataTable{i, j};
        else
            data = DataTable(i, j);
        end
        if isnumeric(data)
            if ~isempty(findstr(class(data), 'int'))
                SQLCommand = [SQLCommand int2str(double(data)) ', '];
            else
                SQLCommand = [SQLCommand num2str(data) ', '];
            end
        elseif islogical(data)
            if data
                SQLCommand = [SQLCommand 'TRUE, '];
            else
                SQLCommand = [SQLCommand 'FALSE, '];
            end
        else
            SQLCommand = [SQLCommand '''' data ''', '];
        end
    end
    SQLCommand = [SQLCommand(1:end-2) ')'];
    %try
        DB.Execute(SQLCommand);
%         ps=DB.prepareStatement(SQLCommand);
        %DB.executeQuery(SQLCommand);
%     catch
%         errorcode = -1;
%         blop = lasterr;
%         warning('CSE:DBLibrary', ['CSE DB Library Error\nUnable to execute command\n"%s":\n%s'], SQLCommand, blop);
% 	end
end
