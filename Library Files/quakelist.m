function [output] = quakelist(number)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

switch number
    
    case 1 
        output = {"Manhole Lid" };
    
    case 2 
        output = "Telus Manhole" ;
    
    case 3 
        output = "Hydro Manhole" ;
    
    case 4 
        output = "Sewer Manhole" ;
    
    case 5 
        output = "Sanitary Manhole" ;
    
    case 6 
        output = "Service Box" ;
    
    case 7 
        output = "Service Vault" ;
    
    case 8 
        output = "Hydro Service Vault" ;
    
    case 9 
        output = "Pilaster" ;
    
    case 10 
        output = "Pole" ;
    
    case 11 
        output = "PVC Conduit" ;
    
    case 12 
        output = "DB2 PVC Bends" ;
    
    case 13 
        output = "Sewer Catch Basin" ;
    
    case 14 
        output = "Fire Hydrant" ;
    
    case 15 
        output = "Property Line" ;
    
    case 16 
        output = "Lot Line" ;
    
    case 17
        output = "Tree" ;
    
    case 18 
        output = "Road Edge" ;
    
    case 19 
        output = "Driveway Edge" ;
    
    case 20 
        output = "Sidewalk Edge" ;
    
    case 21 
        output = "Water Valve" ;
    
    case 22 
        output = "Fence" ;
    
    case 23 
        output = "Other" ;
    

end
end

