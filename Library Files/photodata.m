function [ time ] = photodata( pic )

 data = imfinfo(pic);

datetime = data.DateTime;

fields = textscan(datetime,'%s');
fields = char(fields{1});
fields = fields(2,1:8);
fields = textscan(fields,'%s');
time = fields{:,:};
end

