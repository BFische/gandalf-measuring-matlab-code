clear all; clc;

%% GPX reader requires mapping toolbox
% p = gpxread2('20180313-163843.gpx');
% p2 = gpxread2('20180314-100248.gpx');
% Lat = p.Latitude;
% Lon = p.Longitude;
% geoshow(Lat,Lon,'DisplayType','line','Color','red');
% grid on;


%% NMEA Grabs only the GPGGA data. Saves the time and Lat/Long

[fid,errorMessage] = fopen('20180314153650.txt');
line = cell(1,1);
dataGPS = cell(1,3);
n = 1;
        while 1 
           tline = fgetl(fid);
           if ~ischar(tline), break, end
                      
           Q = nmealineread2(tline);
           if Q.BODCTime ~=0
           dataGPS{n,1} = Q.BODCTime;
           dataGPS{n,2} = Q.latitude;
           dataGPS{n,3} = Q.longitude;
           n=n+1;
           end
           %line{n,1} = tline;
           
       end
       fclose(fid);


% Q = nmealineread2(line);
% time = Q.BODCTime;
% lat = Q.latitude;
% long = Q.longitude;


%fields = textscan(tline,'%s','delimiter',',');

%% For Bodc time to 24 hour clock 
% time = 24*time;
% timeMin = rem(time,1);
% timeHour = time-timeMin;
% timeMin = timeMin*60;
% timeSec = rem(timeMin,1);
% timeMin = timeMin - timeSec;
% timeSec = timeSec*60;


%fclose('all');