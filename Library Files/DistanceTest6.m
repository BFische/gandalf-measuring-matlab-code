% function [ height ] = DistanceTest6(PIC1,PIC2,stereoParamsin)
%% Used to calculate the distance to an object. The first two click should be on the point A and the second two on point B.
% P2 is the distance between point A and B which is normally the height of
% the target.

 
clear all;
warning off;
%  clc;

%% Loads the Calibration and Images (can be commented out if you dont clear your workspace after the first iteration).
stereoParams = load('stereoParams'); %Loads the calibration data 
 
stereoParams = stereoParams.stereoParams;

 PIC1 = 'Left/GOPR6359.JPG';
 PIC2 = 'Right/GOPR6250.JPG';
 
 %PIC1 = '2148003-2.jpg';

 
 PoleID = '';
 %copyfile Left/GOPR1583.JPG 2148003.jpg;
 
%  numofstrands = 1;
%  iter = 5;  % 5-7 
 
%  if ~exist(PoleID, 'dir')
%     mkdir(PoleID);
%  end
 
% [zonenum, zone1]=xlsread('zone1.xlsx');
% savefile = 'zone1.xlsx';

%load('DataCell_Region1.mat');






I1 = imread(PIC1); % reads image left
I2 = imread(PIC2); % reads image right


% info = imfinfo(PIC1);
% info2 = imfinfo(PIC2);
% time1 = info.DateTime;
% time2 = info2.DateTime;
%% Hist filter
%I1 = undistortImage(I1, stereoParamsGO3.CameraParameters1,'OutputView', 'same');
%I2 = undistortImage(I2, stereoParamsGO3.CameraParameters2,'OutputView', 'same');

%filters the rectified images a bit to brighten them and improve contrast
% J1 = histeq(I1);
% J2 = histeq(I2);

J1 = I1;
J2 = I2;

%J1 = imrotate(J1, 90);

%J1 = imrotate(J1, 180);
%J2 = imrotate(J2, 180);

%J1 = rangefilt(J1);
%J2 = rangefilt(J2);



% q = fspecial('unsharp');
% J1 = imfilter(J1,q);
% J2 = imfilter(J2,q);

% J1 = imsharpen(J1); % slightly increases image quality but takes longer
% J2 = imsharpen(J2);
% J1 = I1;
% J2 = I2;




%% Edge Detection
% R1 = J1(:,:,1);
% G1 = J1(:,:,2);
% B1 = J1(:,:,3);
% 
% R2 = J2(:,:,1);
% G2 = J2(:,:,2);
% B2 = J2(:,:,3);
% 
% THRESH = 0.10;
% H = 1;
% 
% BW1 = edge(R1,'sobel',THRESH);
% BW2 = edge(R2,'sobel',THRESH);
% BW3 = edge(G1,'sobel',THRESH);
% BW4 = edge(G2,'sobel',THRESH);
% BW5 = edge(B1,'sobel',THRESH);
% BW6 = edge(B2,'sobel',THRESH);
% 
% 
% J1=BW1+BW3+BW5;
% J2=BW2+BW4+BW6;



%% Gradient Filter
% R1 = J1(:,:,1);
% G1 = J1(:,:,2);
% B1 = J1(:,:,3);
% 
% R2 = J2(:,:,1);
% G2 = J2(:,:,2);
% B2 = J2(:,:,3);
% 
% [J1, M1] = imgradient(R1,'roberts');
% [J2, M2] = imgradient(R2,'roberts');






%% Point Capture
% fig3 = figure('units','normalized','outerposition',[0 0 1 1]);
% zoom on;

%imshowpair(I1, I2, 'montage');

fig = figure('units','normalized','outerposition',[0 0 1 1]);
zoom on;

imshow(J1,'InitialMagnification','fit');
Pick1 = ginput2(1);
imshow(J2,'InitialMagnification','fit');
Pick2 = ginput2(1);


[point3d,error1] = triangulate2(Pick1,Pick2, stereoParams);
A = norm(point3d)/1000;


imshow(J1,'InitialMagnification','fit');
Pick3 = ginput2(1);
imshow(J2,'InitialMagnification','fit');
Pick4 = ginput2(1);
close(fig);

[point3d2,error2] = triangulate2(Pick3,Pick4, stereoParams);
B = norm(point3d2)/1000;


% if numofstrands > 1
%    imshow(J1,'InitialMagnification','fit');
%    Pick5 = ginput2(1);
%    imshow(J2,'InitialMagnification','fit');
%    Pick6 = ginput2(1);
%    close(fig); 
% 
% [point3d3,error3] = triangulate2(Pick5,Pick6, stereoParams);
% C = norm(point3d3)/1000;   
%    
% end

P2 = norm(abs(point3d - point3d2))/1000; %distance


%% Save data to current DataCell

% for i = 1:length(DataCell)
%     if strcmp(DataCell{i,1},PoleID)
%         DataCell{i,2}='Checked';
%         
%         
%         
%     end
% end
%    
% 
% 
% 
% save('DataCell_Region1.mat', 'DataCell');

%% Rastor data

locationC1 = [2736-Pick1(2),Pick1(1),10];
locationC2 = [Pick3(1),Pick3(2),10];
locationC3 = [Pick2(1),Pick2(2),10];
locationC4 = [Pick4(1),Pick4(2),10];
locationC7 = [2745-Pick1(2),Pick1(1)-9,1];


locationC5 = [Pick1,Pick3];
locationC6 = [Pick2,Pick4];

% J1 = imrotate(J1, -90);

distanceAsString = sprintf('%0.2f m', P2);
% J1 = insertObjectAnnotation(J1,'circle',locationC1,distanceAsString,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationC2, 'LineWidth', 3,'Color','blue');
J1 = insertShape(J1, 'line', locationC5, 'LineWidth', 2,'Color','yellow'); 


J1 = imrotate(J1, -90);
J1 = insertObjectAnnotation(J1,'circle',locationC7,distanceAsString,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationC1, 'LineWidth', 3,'Color','blue');

% J2 = insertObjectAnnotation(J2,'circle',locationC3, distanceAsString,'FontSize',72, 'LineWidth', 3,'Color','blue','TextColor','white');
% J2 = insertShape(J2,'circle',locationC4, 'LineWidth', 3,'Color','blue');
% J2 = insertShape(J2, 'line', locationC6, 'LineWidth', 2,'Color','blue');
% 
% if numofstrands > 1
% 
% P3 = norm(abs(point3d - point3d3))/1000; %distance
% 
% end


fig = figure('units','normalized','outerposition',[0 0 1 1]);


imshow(J1,'InitialMagnification','fit');
imsave(fig);
% imshowpair(J1, J2, 'montage');
%close(gcf);%closes the figure


%y = 0.0005858x - 0.0000268  meters to mm off
errorA = (A*0.0005858 -0.0000268)*error1;
errorB = (B*0.0005858 -0.0000268)*error2;


height = P2

% for i = 2:381
%    if zone1{i,1}== PoleID
%        zone1{i,2}=A;
%        zone1{i,3}=B;
%        zone1{i,iter}=round(P2,2);
%        
%    end
%     
%     
% end
    


%% machine learning test
% recycleDetect = vision.CascadeObjectDetector('recycleDetector.xml');
% recycle1 = recycleDetect(J1);
% recycle2 = recycleDetect(J2);
% center1 = recycle1(1:2) + recycle1(3:4)/2;
% center2 = recycle2(1:2) + recycle2(3:4)/2; 
% 
% 
% [point3d3,error3] = triangulate(center1,center2, stereoParams28_2);
% C = norm(point3d3)/1000;
% 
% distanceAsString = sprintf('%0.2f meters', C);
% J1 = insertObjectAnnotation(J1,'rectangle',recycle1,distanceAsString,'FontSize',18);
% J2 = insertObjectAnnotation(J2,'rectangle',recycle2, distanceAsString,'FontSize',18);
% J1 = insertShape(J1,'FilledRectangle',recycle1);
% J2 = insertShape(J2,'FilledRectangle',recycle2);
% 
% imshowpair(J1, J2, 'montage');


% end
% xlswrite(savefile,zone1);