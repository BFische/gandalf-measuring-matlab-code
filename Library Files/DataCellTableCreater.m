function [] = DataCellTableCreater(ProjectNumber)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

%save('DataCell_2715376.mat', 'DataCell');
%DataCell = cell(70,57);
%save('DataCell_2714618.mat', 'DataCell');

DataCell = cell(120,107);
name1 = sprintf('DataCell_%d.mat', ProjectNumber);


%% Column List

DataCell{1,1} = 'pole_id';
DataCell{1,2} = 'height_ag_Distance';
DataCell{1,3} = 'height_ag_Point1';
DataCell{1,4} = 'height_ag_Point2';
DataCell{1,5} = 'strand_0_Distance';
DataCell{1,6} = 'strand_0_Point1';
DataCell{1,7} = 'strand_0_Point2';
DataCell{1,8} = 'strand_1_Distance';
DataCell{1,9} = 'strand_1_Point1';
DataCell{1,10} = 'strand_1_Point2';
DataCell{1,11} = 'strand_2_Distance';
DataCell{1,12} = 'strand_2_Point1';
DataCell{1,13} = 'strand_2_Point2';
DataCell{1,14} = 'strand_3_Distance';
DataCell{1,15} = 'strand_3_Point1';
DataCell{1,16} = 'strand_3_Point2';
DataCell{1,17} = 'strand_4_Distance';
DataCell{1,18} = 'strand_4_Point1';
DataCell{1,19} = 'strand_4_Point2';
DataCell{1,20} = 'strand_5_Distance';
DataCell{1,21} = 'strand_5_Point1';
DataCell{1,22} = 'strand_5_Point2';
DataCell{1,23} = 'neut_1_Distance';
DataCell{1,24} = 'neut_1_Point1';
DataCell{1,25} = 'neut_1_Point2';
DataCell{1,26} = 'sec_1_Distance';
DataCell{1,27} = 'sec_1_Point1';
DataCell{1,28} = 'sec_1_Point2';
DataCell{1,29} = 'sec_2_Distance';
DataCell{1,30} = 'sec_2_Point1';
DataCell{1,31} = 'sec_2_Point2';
DataCell{1,32} = 'sec_3_Distance';
DataCell{1,33} = 'sec_3_Point1';
DataCell{1,34} = 'sec_3_Point2';
DataCell{1,35} = 'sec_4_Distance';
DataCell{1,36} = 'sec_4_Point1';
DataCell{1,37} = 'sec_4_Point2';
DataCell{1,38} = 'sec_5_Distance';
DataCell{1,39} = 'sec_5_Point1';
DataCell{1,40} = 'sec_5_Point2';
DataCell{1,41} = 'sec_6_Distance';
DataCell{1,42} = 'sec_6_Point1';
DataCell{1,43} = 'sec_6_Point2';
DataCell{1,44} = 'guy_1_Distance';
DataCell{1,45} = 'guy_1_Point1';
DataCell{1,46} = 'guy_1_Point2';
DataCell{1,47} = 'guy_2_Distance';
DataCell{1,48} = 'guy_2_Point1';
DataCell{1,49} = 'guy_2_Point2';
DataCell{1,50} = 'guy_3_Distance';
DataCell{1,51} = 'guy_3_Point1';
DataCell{1,52} = 'guy_3_Point2';
DataCell{1,53} = 'guy_4_Distance';
DataCell{1,54} = 'guy_4_Point1';
DataCell{1,55} = 'guy_4_Point2';
DataCell{1,56} = 'a_guy_Distance';
DataCell{1,57} = 'a_guy_Point1';
DataCell{1,58} = 'a_guy_Point2';
DataCell{1,59} = 'a_guy_1_Distance';
DataCell{1,60} = 'a_guy_1_Point1';
DataCell{1,61} = 'a_guy_1_Point2';
DataCell{1,62} = 'a_guy_2_Distance';
DataCell{1,63} = 'a_guy_2_Point1';
DataCell{1,64} = 'a_guy_2_Point2';
DataCell{1,65} = 'a_guy_3_Distance';
DataCell{1,66} = 'a_guy_3_Point1';
DataCell{1,67} = 'a_guy_3_Point2';
DataCell{1,68} = 'a_guy_4_Distance';
DataCell{1,69} = 'a_guy_4_Point1';
DataCell{1,70} = 'a_guy_4_Point2';
DataCell{1,71} = 'primary_1_Distance';
DataCell{1,72} = 'primary_1_Point1';
DataCell{1,73} = 'primary_1_Point2';
DataCell{1,74} = 'primary_2_Distance';
DataCell{1,75} = 'primary_2_Point1';
DataCell{1,76} = 'primary_2_Point2';
DataCell{1,77} = 'surge_arr_Distance';
DataCell{1,78} = 'surge_arr_Point1';
DataCell{1,79} = 'surge_arr_Point2';
DataCell{1,80} = 'transform_Distance';
DataCell{1,81} = 'transform_Point1';
DataCell{1,82} = 'transform_Point2';
DataCell{1,83} = 'self_supp_Distance';
DataCell{1,84} = 'self_supp_Point1';
DataCell{1,85} = 'self_supp_Point2';
DataCell{1,86} = 'st_lgt_Distance';
DataCell{1,87} = 'st_lgt_Point1';
DataCell{1,88} = 'st_lgt_Point2';
DataCell{1,89} = 'switch_Distance';
DataCell{1,90} = 'switch_Point1';
DataCell{1,91} = 'switch_Point2';
DataCell{1,92} = 'sa_bank_Distance';
DataCell{1,93} = 'sa_bank_Point1';
DataCell{1,94} = 'sa_bank_Point2';
DataCell{1,95} = 'i_bank_Distance';
DataCell{1,96} = 'i_bank_Point1';
DataCell{1,97} = 'i_bank_Point2';
DataCell{1,98} = 'misc_Distance';
DataCell{1,99} = 'misc_Point1';
DataCell{1,100} = 'misc_Point2';
DataCell{1,101} = 'Left photo';
DataCell{1,102} = 'Right photo';
DataCell{1,103} = 'Time';
DataCell{1,104} = 'Resolution';
DataCell{1,105} = 'Operator';
DataCell{1,106} = 'MarkerHeight';
DataCell{1,107} = 'Clearances';
DataCell{1,108} = 'datum_Distance';
DataCell{1,109} = 'datum_Point1';
DataCell{1,110} = 'datum_Point2';

DataCell{2,108} = 'datum';
DataCell{2,109} = 'res';
DataCell{2,110} = 'res';
DataCell{2,2} = 'height';
DataCell{2,3} = 'res';
DataCell{2,4} = 'res';
DataCell{2,5} = 'strand 0';
DataCell{2,6} = 'res';
DataCell{2,7} = 'res';
DataCell{2,8} = 'strand 1';
DataCell{2,9} = 'res';
DataCell{2,10} = 'res';
DataCell{2,11} = 'strand 2';
DataCell{2,12} = 'res';
DataCell{2,13} = 'res';
DataCell{2,14} = 'strand 3';
DataCell{2,15} = 'res';
DataCell{2,16} = 'res';
DataCell{2,17} = 'strand 4';
DataCell{2,18} = 'res';
DataCell{2,19} = 'res';
DataCell{2,20} = 'strand 5';
DataCell{2,21} = 'res';
DataCell{2,22} = 'res';
DataCell{2,23} = 'neut 1';
DataCell{2,24} = 'res';
DataCell{2,25} = 'res';
DataCell{2,26} = 'sec 1';
DataCell{2,27} = 'res';
DataCell{2,28} = 'res';
DataCell{2,29} = 'sec 2';
DataCell{2,30} = 'res';
DataCell{2,31} = 'res';
DataCell{2,32} = 'sec 3';
DataCell{2,33} = 'res';
DataCell{2,34} = 'res';
DataCell{2,35} = 'sec 4';
DataCell{2,36} = 'res';
DataCell{2,37} = 'res';
DataCell{2,38} = 'sec 5';
DataCell{2,39} = 'res';
DataCell{2,40} = 'res';
DataCell{2,41} = 'sec 6';
DataCell{2,42} = 'res';
DataCell{2,43} = 'res';
DataCell{2,44} = 'guy 1';
DataCell{2,45} = 'res';
DataCell{2,46} = 'res';
DataCell{2,47} = 'guy 2';
DataCell{2,48} = 'res';
DataCell{2,49} = 'res';
DataCell{2,50} = 'guy 3';
DataCell{2,51} = 'res';
DataCell{2,52} = 'res';
DataCell{2,53} = 'guy 4';
DataCell{2,54} = 'res';
DataCell{2,55} = 'res';
DataCell{2,56} = 'a_guy';
DataCell{2,57} = 'res';
DataCell{2,58} = 'res';
DataCell{2,59} = 'a_guy 1';
DataCell{2,60} = 'res';
DataCell{2,61} = 'res';
DataCell{2,62} = 'a_guy 2';
DataCell{2,63} = 'res';
DataCell{2,64} = 'res';
DataCell{2,65} = 'a_guy 3';
DataCell{2,66} = 'res';
DataCell{2,67} = 'res';
DataCell{2,68} = 'a_guy 4';
DataCell{2,69} = 'res';
DataCell{2,70} = 'res';
DataCell{2,71} = 'primary 1';
DataCell{2,72} = 'res';
DataCell{2,73} = 'res';
DataCell{2,74} = 'primary 2';
DataCell{2,75} = 'res';
DataCell{2,76} = 'res';
DataCell{2,77} = 'surge_arr';
DataCell{2,78} = 'res';
DataCell{2,79} = 'res';
DataCell{2,80} = 'transform';
DataCell{2,81} = 'res';
DataCell{2,82} = 'res';
DataCell{2,83} = 'self_supp';
DataCell{2,84} = 'res';
DataCell{2,85} = 'res';
DataCell{2,86} = 'st_lgt';
DataCell{2,87} = 'res';
DataCell{2,88} = 'res';
DataCell{2,89} = 'switch';
DataCell{2,90} = 'res';
DataCell{2,91} = 'res';
DataCell{2,92} = 'sa_bank';
DataCell{2,93} = 'res';
DataCell{2,94} = 'res';
DataCell{2,95} = 'i_bank';
DataCell{2,96} = 'res';
DataCell{2,97} = 'res';
DataCell{2,98} = 'misc';
DataCell{2,99} = 'res';
DataCell{2,100} = 'res';
DataCell{2,101} = 'Left photo';
DataCell{2,102} = 'Right photo';
DataCell{2,103} = 'Time';
DataCell{2,104} = 'Resolution';
DataCell{2,105} = 'Operator';
DataCell{2,106} = 'MarkerHeight';
DataCell{2,107} = 'Clearances';

save(name1, 'DataCell');
end

