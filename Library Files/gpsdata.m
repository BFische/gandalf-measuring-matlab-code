function [ gpsdata ] = gpsdataconv( datain )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


for line = 1:height(datain)

%time
chars(line,:) = char(table2array(datain(line,2)));    
stamp(line,:) = textscan(chars(line,1:8),'%s');
gpsdata(line,:) = stamp{line,:};

%location    
llh = datain{line,3:5};
cartdata(line,:) = convertLlh2Cart( llh );
gpsdata{line,2} = cartdata(line,:);
end



end

