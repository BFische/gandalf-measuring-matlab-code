function [ cart ] = convertLlh2Cart( llh )

%llh should be in the format [lat, long, height]



% 
%    lat = dms2degrees([48 51 24])
%    lon = dms2degrees([2 21 03]) 

%   h = 80;
 
wgs84 = wgs84Ellipsoid('meters');


[x,y,z] = geodetic2ecef(wgs84,llh(1),llh(2),llh(3));

cart = [x,y,z];




end

