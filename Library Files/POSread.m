function [dataPOS,ierr]  =  POSread(nline)
%% a variation of nmea read that will read pos data sets instead.


ierr  =  0;


%%  Find which string we're dealing with

fields = textscan(nline,'%s','delimiter','%');
% %pull the checksum out of the last field and make a new one for it
fields{1}{end+1} = fields{1}{end}(end-1:end);
% %cut off the old last field at the chksum delimiter
fields{1}(end-1) = strtok(fields{1}(end-1), '*');
% case_t = find(strcmp(fields{1}(1), nmea_options),1);
fields = char(fields{1});

lineAt = 24;
format long;

while 1

        if fields(lineAt) == fields(end)
            break
        end
 
        %first data field is the time
        t_time  =  fields(lineAt,[12 13 15 16 18:23]);
        dataPOS{lineAt,1}  = str2double(t_time);
        clear t_time;
        
        %next data field is the lat
        t_lat  =  fields(lineAt,27:38);
        dataPOS{lineAt,2}  = str2double(t_lat); %...
        
        clear t_lat ;
        
        %then the lon
        t_lon  = fields(lineAt,40:53);
        dataPOS{lineAt,3}  = str2double(t_lon); %...
        clear t_lon t_longDir;
       
        lineAt= lineAt+1;
end

