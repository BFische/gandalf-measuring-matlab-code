function [ llh ] = convertCart2Llh( cart )

%llh should be in the format [lat, long, height]



% 
%    lat = dms2degrees([48 51 24])
%    lon = dms2degrees([2 21 03]) 

%   h = 80;
 
wgs84 = wgs84Ellipsoid('meters');


[lat,long,h] = ecef2geodetic(wgs84,cart(1),cart(2),cart(3));

llh = [lat,long,h];




end


