   BFISCHER 
   ENCEPTA
   11/2/2018

This code base is a MATLAB library used with the volumetric data collection research device. Also known as the Gandalf Staff. 

Initial Versions of this code was created in February of 2018.
The code is still under development.

Current main code is: HOA_integration.m

This code requires a variety of inputs that are laid out in the first 27 lines of the code.

Path setup is not automatic and requires 3 sources.
1. Included all folders and subfolders from:	P:\2018071 - Volumetric Data Collection Research\4 Project Gandalf Staff\5 White Wizard\Integration Prototype\1 Matlabcode\Repo 

2. Include folder:	G:\Team Drives\Technology Team\2018073 - Thunder\GS - Calibration Resources\**PROPER CALIBRATION FOLDER**

3. Include all folders and subfolders from: G:\Team Drives\Technology Team\2018073 - Thunder\**YOUR PROJECT**

The working folder must be:	G:\Team Drives\Technology Team\2018073 - Thunder\**YOUR PROJECT**\Gandalf Staff Photos\




 