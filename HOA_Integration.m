%% Integration Code
%   BFISCHER 
%   ENCEPTA
%   10/24/2018
%   Integration code for Gandalf staff HOA measurements 

clear all;
warning off;

%% Inputs

LeftPicture     =	'0759'          ; %Left Photo Number
RightPicture    =	'9411'          ; %Right Photo Number

PoleID          =	"1234590"       ;



PhotoEnhancment =   0              ; % 0: nothing, 1:Hist filter, #>10: that much increase in brightness, best is around 35

ProjectNumber   =   '7654321'       ; %the seven digit project number

MarkerHeight    =   0               ; %If a marker was used to define the datum

Comments        =   ''              ;

SaveProject        =   'save'          ;% disable if you dont want to save the dataset

%% Commands 

%DataCellTableCreater(ProjectNumber)

[DataCell,I1] = Pole_Measure_Tool_V7(LeftPicture,RightPicture,PoleID,ProjectNumber,PhotoEnhancment,MarkerHeight,SaveProject);

DataCell = HOACalculator(PoleID,'whole',SaveProject,DataCell,ProjectNumber);

[MeasuredPIC] = ImageCreater(PoleID,'single',SaveProject,DataCell,I1);

fig = figure('units','normalized','outerposition',[0 0 1 1]);
imshow(MeasuredPIC,'InitialMagnification','fit');
imagefilename = sprintf('%s.jpg',PoleID);
imwrite(MeasuredPIC,imagefilename); 

%OutputSheetCreator(ProjectNumber,DataCell);

