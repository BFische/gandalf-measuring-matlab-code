clear all;
warning off;

%altered for HOA 
%allow for downguys to be copied from the strands
%remove 0 marker heights

%allow for single point clicking to be done after the base and the top have
%been taken. Allow for a different section to be taken as the line marker
%instead.

%have the numbers pushed to the side and in order

%% Load the Calibration and Images
stereoParams = load('stereoParamsOnt'); %Loads the calibration data 
stereoParams = stereoParams.stereoParamsOnt;

% StartLeft = 0;
% StartRight= 0;
% EndLeft = 9;
% EndRight = 9;
% [PICSLeft,PICSRight] = photoloader( StartLeft,EndLeft,StartRight,EndRight);
% 
% % for k=1:10
%   PIC1 = PICSLeft(k,:);
%   PIC2 = PICSRight(k,:);
 

PIC1 = 'Left/GOPR6220.JPG';
PIC2 = 'Right/GOPR6104.JPG';


PoleID = ""; 
MarkerHeight =  0;  % either 0.75 or 1.37 m




Comments = ''; 
infoPIC1 = imfinfo(PIC1);
NativeResolution = '3648 x 2736 Pixels';
TimeStamp= infoPIC1.DateTime; %of both the change and the original photos. 
NameofOperator= 'Brian Fischer';


load('DataCell_Region.mat');


checkflag=0;
for i = 1:length(DataCell)
   if strcmp(DataCell{i,1},PoleID)
       checkflag=1;


I1 = imread(PIC1); % reads image left
I2 = imread(PIC2); % reads image right

%% Filter Images
%no filter
J1 = I1; 
J2 = I2;


%Hist Filter
J1 = histeq(J1); 
J2 = histeq(J2);
clear I2 I1;
%Rotate images if they are flipped
%J1 = imrotate(J1, 180); 
%J2 = imrotate(J2, 180);

%imshow(J1,'InitialMagnification','fit');
  

    

%% Section 1: Pick Datum

DatumCheck = input('Choose Datum Point? y/n: [y] ', 's');
if isempty(DatumCheck)
    DatumCheck = 'y';
end

if strcmp(DatumCheck,'y')
    %grab datum point
    [DatumDistance, Datum1, Datum2 ] = Measure_Points( J1, J2, stereoParams);
    
    DataCell{i,2}=DatumDistance;
    DataCell{i,3}=Datum1;
    DataCell{i,4}=Datum2;
    
   


else
% return %% to cancel
    %do nothing and continue
end



%% Section 2: Pole Height

PoleHeightCheck = input('Can you take the pole height? y/n: [y] ', 's');
if isempty(PoleHeightCheck)
    PoleHeightCheck = 'y';
end

if strcmp(PoleHeightCheck,'y')
    %grab top point
    [TopDistance, Top1, Top2 ] = Measure_Points( J1, J2, stereoParams);
%     %grab bottom point
%     [BottomDistance, Bottom1, Bottom2 ] = Measure_Points( J1, J2, stereoParams);
    
    DataCell{i,5}=TopDistance;
    DataCell{i,6}=Top1;
    DataCell{i,7}=Top2;
%     DataCell{i,8}=DatumDistance;
%     DataCell{i,9}=Datum1;
%     DataCell{i,10}=Datum2;
    



else
%return
    %do nothing and continue
end

 
%% Section 3: Pick J Hooks CHANGED TO HYDRO LINES

NumberofJHooks = input('Number of Strands? [1]: ');
if isempty(NumberofJHooks)
    NumberofJHooks = 1;
end

if NumberofJHooks >0
    [JHook1Distance, JHook1_1, JHook1_2 ] = Measure_Points( J1, J2, stereoParams);
    DataCell{i,11}=JHook1Distance;
    DataCell{i,12}=JHook1_1;
    DataCell{i,13}=JHook1_2;  
    
    if NumberofJHooks >1
        [JHook2Distance, JHook2_1, JHook2_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,14}=JHook2Distance;
        DataCell{i,15}=JHook2_1;
        DataCell{i,16}=JHook2_2;
        
        if NumberofJHooks >2
            [JHook3Distance, JHook3_1, JHook3_2 ] = Measure_Points( J1, J2, stereoParams);
            DataCell{i,17}=JHook3Distance;
            DataCell{i,18}=JHook3_1;
            DataCell{i,19}=JHook3_2;
            
            
            
            
        end
    end
    
               
end



%% Section 4: Strands
NumberofStrands = input('Number of Secondary Lines? [1]: ');
if isempty(NumberofStrands)
    NumberofStrands = 1;
end

if NumberofStrands >0
    [Strand1Distance, Strand1_1, Strand1_2 ] = Measure_Points( J1, J2, stereoParams);
    DataCell{i,20}=Strand1Distance;
    DataCell{i,21}=Strand1_1;
    DataCell{i,22}=Strand1_2;
        
    if NumberofStrands >1
        CopyData2= input('Secondary Line 2, copy last data? y/n [n]: ', 's');
        if isempty(CopyData2)
            CopyData2 = 'n';
        end
        if strcmp(CopyData2,'y')
        DataCell{i,23}=DataCell{i,20}; Strand2_1=Strand1_1;
        DataCell{i,24}=DataCell{i,21}; 
        DataCell{i,25}=DataCell{i,22};   
            
        else
        [Strand2Distance, Strand2_1, Strand2_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,23}=Strand2Distance;
        DataCell{i,24}=Strand2_1;
        DataCell{i,25}=Strand2_2;
        end
        if NumberofStrands >2
            CopyData3= input('Secondary Line 3, copy last data? y/n [n]: ', 's');
            if isempty(CopyData3)
                CopyData3 = 'n';
            end
            if strcmp(CopyData3,'y')
            DataCell{i,26}=DataCell{i,23}; Strand3_1=Strand2_1;
            DataCell{i,27}=DataCell{i,24};
            DataCell{i,28}=DataCell{i,25};   
            
            else
            [Strand3Distance, Strand3_1, Strand3_2 ] = Measure_Points( J1, J2, stereoParams);   
            DataCell{i,26}=Strand3Distance;
            DataCell{i,27}=Strand3_1;
            DataCell{i,28}=Strand3_2;
            end
            if NumberofStrands >3
                CopyData4= input('Secondary Line 4, copy last data? y/n [n]: ', 's');
                if isempty(CopyData4)
                    CopyData3 = 'n';
                end
                
                if strcmp(CopyData4,'y')
                DataCell{i,29}=DataCell{i,26}; Strand4_1=Strand3_1;
                DataCell{i,30}=DataCell{i,27};
                DataCell{i,31}=DataCell{i,28};
                
                else
                [Strand4Distance, Strand4_1, Strand4_2 ] = Measure_Points( J1, J2, stereoParams);
                DataCell{i,29}=Strand4Distance;
                DataCell{i,30}=Strand4_1;
                DataCell{i,31}=Strand4_2;
                end
            end
        end
    end
end

clear CopyData2 CopyData3 CopyData4;


%% Section 5: Guys
NumberofGuys = input('Number of Guys? [0]: ');
if isempty(NumberofGuys)
    NumberofGuys = 0;
end

if NumberofGuys >0
    CopyData1= input('Guy 1, copy first strand data? y/n [n]: ', 's');
        if isempty(CopyData1)
            CopyData1 = 'n';
        end
        if strcmp(CopyData1,'y')
        DataCell{i,32}=DataCell{i,20}; Guy1_1=Strand1_1;
        DataCell{i,33}=DataCell{i,21};
        DataCell{i,34}=DataCell{i,22};   
            
        else
    [Guy1Distance, Guy1_1, Guy1_2 ] = Measure_Points( J1, J2, stereoParams);
    DataCell{i,32}=Guy1Distance;
    DataCell{i,33}=Guy1_1;
    DataCell{i,34}=Guy1_2;
        
    if NumberofGuys >1
        CopyData2= input('Guy 2, copy last data? y/n [n]: ', 's');
        if isempty(CopyData2)
            CopyData2 = 'n';
        end
        if strcmp(CopyData2,'y')
        DataCell{i,35}=DataCell{i,32}; Guy2_1=Guy1_1;
        DataCell{i,36}=DataCell{i,33};
        DataCell{i,37}=DataCell{i,34};   
            
        else
        [Guy2Distance, Guy2_1, Guy2_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,35}=Guy2Distance;
        DataCell{i,36}=Guy2_1;
        DataCell{i,37}=Guy2_2;
        
        if NumberofGuys >2
            CopyData3= input('Guy 3, copy last data? y/n [n]: ', 's');
            if isempty(CopyData3)
             CopyData3 = 'n';
            end
            if strcmp(CopyData3,'y')
            DataCell{i,38}=DataCell{i,35}; Guy3_1=Guy2_1;
            DataCell{i,39}=DataCell{i,36};
            DataCell{i,40}=DataCell{i,37};   
            
            else
            [Guy3Distance, Guy3_1, Guy3_2 ] = Measure_Points( J1, J2, stereoParams);
            DataCell{i,38}=Guy3Distance;
            DataCell{i,39}=Guy3_1;
            DataCell{i,40}=Guy3_2;
        
    
            end
            end
        end
        end
    end
    
clear CopyData2 CopyData3 CopyData4;   

end

%% Section 6: Comspace
% 
%  Numberofcoms = input('How many devices on the pole? [0]: ');
%  if isempty(Numberofcoms)
%      Numberofcoms = 0;
%  end
%  if Numberofcoms >0
%     [com1Distance, com1_1, com1_2 ] = Measure_Points( J1, J2, stereoParams);
%     DataCell{i,41}=com1Distance;
%     DataCell{i,42}=com1_1;
%     DataCell{i,43}=com1_2;
%         
%     if Numberofcoms >1
%         CopyData2= input('Device 2, copy last data? y/n [n]: ', 's');
%             if isempty(CopyData2)
%              CopyData2 = 'n';
%             end
%             if strcmp(CopyData2,'y')
%             DataCell{i,44}=DataCell{i,41};
%             DataCell{i,45}=DataCell{i,42};
%             DataCell{i,46}=DataCell{i,43};   
%             
%             else
%         
%         
%         [com2Distance, com2_1, com2_2 ] = Measure_Points( J1, J2, stereoParams);
%         DataCell{i,44}=com2Distance;
%         DataCell{i,45}=com2_1;
%         DataCell{i,46}=com2_2;
%             end
%     end
%     clear CopyData2 ;
%  end

 %% Primary
 NumberofPrimary = input('How many Primary Lines on the pole? [1]: ');
 if isempty(NumberofPrimary)
     NumberofPrimary = 1;
 end
 if NumberofPrimary >0
    [prim1Distance, prim1_1, prim1_2 ] = Measure_Points( J1, J2, stereoParams);
    DataCell{i,41}=prim1Distance;
%     DataCell{i,42}=com1_1;
%     DataCell{i,43}=com1_2;
        
    if NumberofPrimary >1
        CopyData2= input('Primary Line 2, copy last data? y/n [n]: ', 's');
            if isempty(CopyData2)
             CopyData2 = 'n';
            end
            if strcmp(CopyData2,'y')
            DataCell{i,44}=DataCell{i,41};
%             DataCell{i,45}=DataCell{i,42};
%             DataCell{i,46}=DataCell{i,43};   
            
            else
        
        
        [prim2Distance, prim2_1, prim2_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,44}=prim2Distance;
%         DataCell{i,45}=com2_1;
%         DataCell{i,46}=com2_2;
            end
    end
    clear CopyData2 ;
 end
 
 %% Equipment on pole
 
  
 NumberofEquip = input('How many pole mounted equipment on the pole? SA 1, TF 2, Misc 3 [0]: ');
 if isempty(NumberofEquip)
     NumberofEquip = 0;
 end
 if NumberofEquip >0  %Surge Arrestor
    SA = input('Surge Arrestor? [1]: ');
        if isempty(SA)
             SA = 1;
        end
        if SA >0
    [equip1Distance, equip1_1, equip1_2 ] = Measure_Points( J1, J2, stereoParams);
    DataCell{i,55}=equip1Distance;
        else
         equip1Distance = DatumDistance;
         equip1_1 = Datum1;
         equip1_2 = Datum2;
        end
    if NumberofEquip >1 %Transforomer
           TF = input('Transformer? [1]: ');
        if isempty(TF)
             TF = 1;
        end
        if TF >0
        [equip2Distance, equip2_1, equip2_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,56}=equip2Distance;
        else
         equip2Distance = DatumDistance;
         equip2_1 = Datum1;
         equip2_2 = Datum2;
        end
        
        if NumberofEquip >2 %Misc
           Misc = input('Other? [1]: ');
            if isempty(Misc)
             Misc = 1;
            end
            if Misc >0
        [equip3Distance, equip3_1, equip3_2 ] = Measure_Points( J1, J2, stereoParams);
        DataCell{i,57}=equip3Distance; 
            else
         equip3Distance = DatumDistance;
         equip3_1 = Datum1;
         equip3_2 = Datum2;
            end
        
        end
    end
 end
     
%% Section 7: Intersection Strands

% IntersectionPole = input('Is this an intersection pole? y/n [n]: ', 's');
%  if isempty(IntersectionPole)
%      IntersectionPole = 'n';
%  end
%  if strcmp(IntersectionPole,'y')
%      Newface = input('Whats is the new bolt face? N/W/E/S [N] ', 's');
%      if isempty(Newface)
%      Newface = 'N';
%      end

%% Section 8: Documentation, Photos names and Comments

DataCell{i,47}=PIC1;
DataCell{i,48}=PIC2;
DataCell{i,49}=TimeStamp;
DataCell{i,50}=NativeResolution;
DataCell{i,52}=NameofOperator;
DataCell{i,53}=Comments;
    
%% Point Display 

Clearances(1) = MarkerHeight+ NORMAL( DataCell{i,2}, DataCell{i,5} ); %Height
Clearances(2) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,11}); %Strands
Clearances(3) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,14}); 
Clearances(4) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,17});
Clearances(5) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,20}); %Secondary
Clearances(6) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,23});
Clearances(7) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,26});
Clearances(8) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,29});
Clearances(9) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,32}); %Guys
Clearances(10) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,35});
Clearances(11) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,38});
Clearances(12) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,41}); %Primary
Clearances(13) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,44});
Clearances(14) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,55}); %Equipment SA
Clearances(15) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,56}); %TR
Clearances(16) = MarkerHeight+ NORMAL(DataCell{i,2} , DataCell{i,57}); %Misc
DataCell{i,54}=Clearances;
%Datum location
locationDat = [Datum1(1),Datum1(2),10];

%Top of pole location
if strcmp(PoleHeightCheck,'y')
locationTop = [2736-Top1(2),Top1(1),10];
locationTop2 = [2745-Top1(2),Top1(1)-9,1];
distanceTop = sprintf('%0.2f m', Clearances(1));
% 
% %Bottom of pole location
% locationBot = [Bottom1(1),Bottom1(2),10];
end
a
%J hook locations
if NumberofJHooks >0
locationJhook1 = [2736-JHook1_1(2),JHook1_1(1),10];
locationJhook1_2 = [2745-JHook1_1(2),JHook1_1(1)-9,1];
distanceJH1 = sprintf('%0.2f m', Clearances(2));
if NumberofJHooks >1
locationJhook2 = [2736-JHook2_1(2),JHook2_1(1),10];
locationJhook2_2 = [2745-JHook2_1(2),JHook2_1(1)-9,1];
distanceJH2 = sprintf('%0.2f m', Clearances(3));
if NumberofJHooks >2
locationJhook3 = [2736-JHook3_1(2),JHook3_1(1),10];
locationJhook3_2 = [2745-JHook3_1(2),JHook3_1(1)-9,1];
distanceJH3 = sprintf('%0.2f m', Clearances(4));
end
end
end

%Strand Locations
if NumberofStrands >0
locationStrand1 = [2736-Strand1_1(2),Strand1_1(1),10];
locationStrand1_2 = [2745-Strand1_1(2),Strand1_1(1)-9,1];
distanceStrand1 = sprintf('%0.2f m', Clearances(5));
if NumberofStrands >1
locationStrand2 = [2736-Strand2_1(2),Strand2_1(1),10];
locationStrand2_2 = [2745-Strand2_1(2),Strand2_1(1)-9,1];
distanceStrand2 = sprintf('%0.2f m', Clearances(6));
if NumberofStrands >2
locationStrand3 = [2736-Strand3_1(2),Strand3_1(1),10];
locationStrand3_2 = [2745-Strand3_1(2),Strand3_1(1)-9,1];
distanceStrand3 = sprintf('%0.2f m', Clearances(7));
if NumberofStrands >3
locationStrand4 = [2736-Strand4_1(2),Strand4_1(1),10];
locationStrand4_2 = [2745-Strand4_1(2),Strand4_1(1)-9,1];
distanceStrand4 = sprintf('%0.2f m', Clearances(8));
end
end
end
end

%Guy Locations
if NumberofGuys >0
locationGuy1 = [2736-Guy1_1(2),Guy1_1(1),10];
locationGuy1_2 =[2745-Guy1_1(2),Guy1_1(1)-9,1];
distanceGuy1 = sprintf('%0.2f m', Clearances(9));
if NumberofGuys >1
locationGuy2 = [2736-Guy2_1(2),Guy2_1(1),10];
locationGuy2_2 =[2745-Guy2_1(2),Guy2_1(1)-9,1];
distanceGuy2 = sprintf('%0.2f m', Clearances(10));
if NumberofGuys >2
locationGuy3 = [2736-Guy3_1(2),Guy3_1(1),10];
locationGuy3_2 =[2745-Guy3_1(2),Guy3_1(1)-9,1];
distanceGuy3 = sprintf('%0.2f m', Clearances(11));
end
end
end

%Primary Locations
if NumberofPrimary >0
locationPrim1 = [2736-prim1_1(2),prim1_1(1),10];
locationPrim1_2 =[2745-prim1_1(2),prim1_1(1)-9,1];
distancePrim1 = sprintf('%0.2f m', Clearances(12));
if NumberofPrimary >1
locationPrim2 = [2736-prim2_1(2),prim2_1(1),10];
locationPrim2_2 =[2745-prim2_1(2),prim2_1(1)-9,1];
distancePrim2 = sprintf('%0.2f m', Clearances(13));
end
end

%Equip Locations
if NumberofEquip >0
locationEquip1 = [2736-equip1_1(2),equip1_1(1),10];
locationEquip1_2 =[2745-equip1_1(2),equip1_1(1)-9,1];
distanceEquip1 = sprintf('%0.2f m', Clearances(14));
if NumberofEquip >1
locationEquip2 = [2736-equip2_1(2),equip2_1(1),10];
locationEquip2_2 =[2745-equip2_1(2),equip2_1(1)-9,1];
distanceEquip2 = sprintf('%0.2f m', Clearances(15));
if NumberofEquip >2
locationEquip3 = [2736-equip3_1(2),equip3_1(1),10];
locationEquip3_2 =[2745-equip3_1(2),equip3_1(1)-9,1];
distanceEquip3 = sprintf('%0.2f m', Clearances(16));
end
end
end
%% Paste to J1
%Lines
%Height
if strcmp(PoleHeightCheck,'y')
locationHeight = [Datum1,Top1];
J1 = insertShape(J1, 'line', locationHeight, 'LineWidth', 2,'Color','yellow');
end
%Jhooks
if NumberofJHooks >0
location = [Datum1,JHook1_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofJHooks >1
location = [Datum1,JHook2_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofJHooks >2
location = [Datum1,JHook3_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
end
end
end

%Strands
if NumberofStrands >0
location = [Datum1,Strand1_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofStrands >1
location = [Datum1,Strand2_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofStrands >2
location = [Datum1,Strand3_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofStrands >3
location = [Datum1,Strand4_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
end
end
end
end

%Guys
if NumberofGuys >0
location = [Datum1,Guy1_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofGuys >1
location = [Datum1,Guy2_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofGuys >2
location = [Datum1,Guy3_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
end
end
end

%Primary
if NumberofPrimary >0
location = [Datum1,prim1_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofPrimary >1
location = [Datum1,prim2_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofPrimary >2
location = [Datum1,prim3_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
end
end
end

%Equipment
if NumberofEquip >0
location = [Datum1,equip1_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofEquip >1
location = [Datum1,equip2_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
if NumberofEquip >2
location = [Datum1,equip3_1];
J1 = insertShape(J1, 'line', location, 'LineWidth', 2,'Color','yellow');
end
end
end

%Circles
J1 = insertShape(J1,'circle', locationDat, 'LineWidth', 3,'Color','blue');
% if strcmp(PoleHeightCheck,'y')
% J1 = insertShape(J1,'circle', locationBot, 'LineWidth', 3,'Color','blue');
% end

%Circels and Distances
J1 = imrotate(J1, -90);
%Height
if strcmp(PoleHeightCheck,'y')
J1 = insertObjectAnnotation(J1,'circle',locationTop2,distanceTop,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle',locationTop, 'LineWidth', 3,'Color','blue');
end
%Jhooks
if NumberofJHooks >0
J1 = insertObjectAnnotation(J1,'circle',locationJhook1_2,distanceJH1,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle',locationJhook1, 'LineWidth', 3,'Color','blue');
if NumberofJHooks >1
J1 = insertObjectAnnotation(J1,'circle',locationJhook2_2,distanceJH2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationJhook2, 'LineWidth', 3,'Color','blue');
if NumberofJHooks >2
J1 = insertObjectAnnotation(J1,'circle',locationJhook3_2,distanceJH3,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationJhook3, 'LineWidth', 3,'Color','blue');
end
end
end
%Strands
if NumberofStrands >0
J1 = insertObjectAnnotation(J1,'circle',locationStrand1_2,distanceStrand1,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationStrand1, 'LineWidth', 3,'Color','blue');
if NumberofStrands >1
J1 = insertObjectAnnotation(J1,'circle',locationStrand2_2,distanceStrand2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationStrand2, 'LineWidth', 3,'Color','blue');
if NumberofStrands >2
J1 = insertObjectAnnotation(J1,'circle',locationStrand3_2,distanceStrand3,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationStrand3, 'LineWidth', 3,'Color','blue');
if NumberofStrands >3
J1 = insertObjectAnnotation(J1,'circle',locationStrand4_2,distanceStrand4,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationStrand4, 'LineWidth', 3,'Color','blue');
end
end
end
end
%Guy
if NumberofGuys >0
J1 = insertObjectAnnotation(J1,'circle',locationGuy1_2,distanceGuy1,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationGuy1, 'LineWidth', 3,'Color','blue');
if NumberofGuys >1
J1 = insertObjectAnnotation(J1,'circle',locationGuy2_2,distanceGuy2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationGuy2, 'LineWidth', 3,'Color','blue');
if NumberofGuys >2
J1 = insertObjectAnnotation(J1,'circle',locationGuy3_2,distanceGuy3,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationGuy3, 'LineWidth', 3,'Color','blue');
end
end
end
%Com
if NumberofPrimary >0
J1 = insertObjectAnnotation(J1,'circle',locationPrim1_2,distancePrim1,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationPrim1, 'LineWidth', 3,'Color','blue');
if NumberofPrimary >1
J1 = insertObjectAnnotation(J1,'circle',locationPrim2_2,distancePrim2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationPrim2, 'LineWidth', 3,'Color','blue');
end
end

%Equip
if NumberofEquip >0
J1 = insertObjectAnnotation(J1,'circle',locationEquip1_2,distanceEquip1,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationEquip1, 'LineWidth', 3,'Color','blue');
if NumberofEquip >1
J1 = insertObjectAnnotation(J1,'circle',locationEquip2_2,distanceEquip2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationEquip2, 'LineWidth', 3,'Color','blue');
if NumberofEquip >2
J1 = insertObjectAnnotation(J1,'circle',locationEquip2_2,distanceEquip2,'FontSize',55,  'LineWidth', 3,'Color','blue','TextColor','white');
J1 = insertShape(J1,'circle', locationEquip2, 'LineWidth', 3,'Color','blue');
end
end
end
%% Display J1
fig = figure('units','normalized','outerposition',[0 0 1 1]);


imshow(J1,'InitialMagnification','fit');
imsave(fig);



 


end
   end
if i == length(DataCell) && ~strcmp(DataCell{i,1},PoleID) && checkflag == 0 
     
       TEMPID{1}=PoleID;
       DataCell(length(DataCell)+1,1)=TEMPID;
       disp('Pole Added: Run Again');
end      




%end %Remove


save('DataCell_Region.mat', 'DataCell');
disp('Saved ');
disp(PoleID);

