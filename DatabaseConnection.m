clear all;

% Add jar file to classpath (ensure it is present in your current dir)
javaclasspath('postgresql-9.0-801.jdbc4.jar');

% Username and password you chose when installing postgres
props=java.util.Properties;
props.setProperty('user', 'bfischer');
props.setProperty('password', 'temp123');

% Create the database connection (port 5432 is the default postgres chooses
% on installation)
driver=org.postgresql.Driver;
url = 'jdbc:postgresql://192.168.128.9:5432/matlab_testing_coms';
conn=driver.connect(url, props);

% A test query
%  sql='select col3 from test_table_1'; % query

data = {1,2,3,4,5;6,7,8,9,0};
text = jsonencode(data)

sql= sprintf( 'update test_table_1 set exp_res_mat = "%s" where number = []', text) ; % query
%sql= 'insert into test_table_1 (number) values (1,2,3,4,5,6)' ;
ps=conn.prepareStatement(sql);
rs=ps.executeQuery();

% Read the results into an array of result structs
count=0;
%result=struct;
while rs.next()
    count=count+1;
    
    result(count)=(rs.getInt(1));  %rs.getString(1) is the entire first column
%     result(count).var2=char(rs.getString(2));
%     result(count).var3=char(rs.getString(4));
%     result(count).var4=char(rs.getString(5));
end
result = result';

% c = database('jdbc:jtds:sqlserver://servername.mydomain.org:1433/mydatabase', 'someusername', 'supersecretpassword');
% v = jdbcquery(c, sql);
% c.close();

%DataTable = ones(5);
%DBWrite(conn, "test_table_1", DataTable)


% %% PGMEX Testing
% %warning ('off');
% import com.allied.pgmex.pgmexec;
% dbConn=com.allied.pgmex.pgmexec('connect','host=192.168.128.9 user=bfischer port=5432 password=temp123 dbname=matlab_testing_coms');
% 
% %create schema and table
% pgmexec('exec',dbConn,'CREATE SCHEMA IF NOT EXISTS testschema');
% %pgmexec('exec',dbConn,'DROP TABLE IF EXISTS testschema.test_table');
% pgResult=pgmexec('exec',dbConn,['CREATE TABLE IF NOT EXISTS testschema.testt ('...
%     't_date date,'...
%     'exp_name varchar(40),'...
%     'exp_conf xml,'...
%     'exp_t_vec timestamp[],'...
%     'exp_col_name_vec name[],'...
%     'exp_res_mat float8[])']);
% 
% pgmexec('resultStatus',pgResult)% ans = 1
% pgmexec('cmdStatus',pgResult)% ans= 'CREATE TABLE'
% %% clear result
% pgmexec('clear',pgResult)
% 
% pgParam=pgmexec('paramCreate',dbConn)% pgParam =uint64(3843492816)
% %% put values of parameters
% pgmexec('putf',pgParam,...
%     '%date %varchar %xml %timestamp[]@ %name[]@ %float8[]',...
%     datenum('1-Dec-2016'),'Experiment #1',...
%     ['<model type="struct" ><param1 type="double" >1</param1>'...
%     '<param2 type="boolean" >false</param2></model>'],...
%     datenum({'0:00:00';'0:00:10';'0:01:30'}),{'res1','res2'},...
%     struct('valueMat',{[NaN 2.4;-2 Inf;0 NaN]},...
%     'isNullMat',{[true false;false false;true false]},...
%     'isValueNull',false));
% % insert the correspoding tuple into the table
% pgmexec('paramExec',dbConn,pgParam,...
%     'INSERT INTO testschema.testt values ($1, $2, $3, $4, $5, $6)');
% % reset parameters once again
% pgmexec('paramreset',pgParam)
% %Finish and close connection
% %com.allied.pgmex.pgmexec('finish',dbConn)


%text = jsonencode(data)